import boto3
import time
import json
from decimal import Decimal


class Database:

    def __init__(self):
       # Get the service resource.
        self.dynamodb = boto3.resource('dynamodb')

        # Instantiate a table resource object without actually
        # creating a DynamoDB table. Note that the attributes of this table
        # are lazy-loaded: a request is not made nor are the attribute
        # values populated until the attributes
        # on the table resource are accessed or its load() method is called.
        self.base_table = self.dynamodb.Table('bsm_data')
        self.agg_table = self.dynamodb.Table('bsm_agg_data')
        self.alerts_table = self.dynamodb.Table('bsm_alerts')
    
    # This method finds a single document using field information provided in the key parameter
    # It assumes that the key returns a unique document. It returns None if no document is found
    def get_item(self, key):
        response = self.base_table.get_item(
            Key={key}
        )
        item = response['Item']
        return item

    # This method inserts the data in a new document. It assumes that any uniqueness check is done by the caller
    def put_item(self, item):
       self.agg_table.put_item(Item={item})


    def alerts_put_item(self, item):
        data = json.loads(json.dumps(item), parse_float=Decimal)
        self.alerts_table.put_item(Item=data)