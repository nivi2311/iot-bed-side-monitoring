import boto3
import time
import json
import decimal
from decimal import Decimal
from database import Database
from boto3.dynamodb.conditions import Key, Attr
import pandas as pd
import datetime

class DecimalEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, decimal.Decimal):
            if o % 1 > 0:
                return float(o)
            else:
                return int(o)
        return super(DecimalEncoder, self).default(o)

# MinutelyAggregateModel class is used to aggregate bed side monitoring data every minute per device per sensor.
class MinutelyAggregateModel:
    def __init__(self):
        self._db = Database()
        self._latest_error = ''     
        self.base_table = 'bsm_data'
        self.agg_table = 'bsm_agg_data'

    
    # This function will fetch the past minute data for all devices from dynamodb
    def get_per_sensor_per_minute_data(self, from_time, to_time):
        """
         Input : Time range - from_time, to_time
         Output : bsm data in the given time range
        """
        table = self._db.base_table
        scan_kwargs = {
            'FilterExpression': Key('timestamp').between(from_time, to_time),
            'ProjectionExpression': "#ts, deviceid, #va, datatype",
            'ExpressionAttributeNames': {"#ts": "timestamp", "#va": "value"}
        }
        data = []
        done = False
        start_key = None
        while not done:
            if start_key:
                scan_kwargs['ExclusiveStartKey'] = start_key
            response = table.scan(**scan_kwargs)
            for i in response['Items']:
                data.append(json.dumps(i, cls=DecimalEncoder))
            start_key = response.get('LastEvaluatedKey', None)
            done = start_key is None
        return data
    
    # Once the data is fetched from dynamodb, the aggregations(min, max, average) is calculated
    # for each device per sensor type
    def calculate_aggregates_per_sensor_per_minute(self, from_time, to_time):
        """
         Input : Time range
         Output : Aggregates per device per sensor type
        """
        data = self.get_per_sensor_per_minute_data(from_time, to_time)
        value_list = []
        for d in data:
            record = json.loads(d)
            value_list.append(record)
        df = pd.DataFrame(value_list)
        aggregates = df.groupby(["deviceid", "datatype"])['value'].describe().reset_index()
        start_time = datetime.datetime.strptime(from_time, '%Y-%m-%d %H:%M:%S')
        timestamp = []
        delta = datetime.timedelta(seconds=1)

        for i in range (len(aggregates.index)):
            time = start_time.strftime('%Y-%m-%d %H:%M:%S')
            timestamp.append(time)
            start_time +=delta

        aggregates['timestamp'] = timestamp
        agg_data = aggregates[["deviceid", "datatype", "timestamp", "mean", "min", "max"]]
        print(agg_data)
        return agg_data
    

    # This function will fetch all data at once and calculate the aggregates for every minute
    # and store it in bsm_agg_data
    def calculate_aggregates_single_fetch(self, from_time, to_time):
        """
        Input : Time range
        Output : Aggregates per device per sensor type
        """
        data = self.get_per_sensor_per_minute_data(from_time, to_time)
        value_list = []
        for d in data:
            record = json.loads(d)
            value_list.append(record)
        df = pd.DataFrame(value_list)
        print(value_list)

        start_time = datetime.datetime.strptime(from_time, '%Y-%m-%d %H:%M:%S')
        start_time = start_time - datetime.timedelta(minutes=start_time.minute % 1,
                             seconds=start_time.second)
        end_time = datetime.datetime.strptime(to_time, '%Y-%m-%d %H:%M:%S')
        end_time = end_time - datetime.timedelta(minutes=end_time.minute % 1,
                             seconds=end_time.second)
        print(start_time)
        print(end_time)
        delta = datetime.timedelta(minutes=1)
        agg_data_list = pd.DataFrame()
        while start_time < end_time:
            to_time = start_time + delta
            df_agg = df.loc[(df['timestamp'] >= start_time.strftime('%Y-%m-%d %H:%M:%S')) & (df['timestamp'] < to_time.strftime('%Y-%m-%d %H:%M:%S'))]
            print(df_agg)
            aggregates = df_agg.groupby(["deviceid", "datatype"])['value'].describe().reset_index()
            print(aggregates)
            
            #start = datetime.datetime.strptime(start_time, '%Y-%m-%d %H:%M:%S')
            start = start_time
            timestamp = []
            delta_sec = datetime.timedelta(seconds=1)

            for i in range (len(aggregates.index)):
                time = start.strftime('%Y-%m-%d %H:%M:%S')
                timestamp.append(time)
                start +=delta_sec

            aggregates['timestamp'] = timestamp
            agg_data = aggregates[["deviceid", "datatype", "timestamp", "mean", "min", "max"]]
            print(agg_data)
            agg_data_list = pd.concat([agg_data_list, aggregates])

            start_time += delta

        
        print(agg_data_list)
        return agg_data_list


     # This function will retrieve and insert the aggregates for each sensor type
    # (HeartRate, Temperature, SPO2) and for each device and insert it into bsm_agg_data
    def insert_aggregates_single_fetch(self, from_time, to_time):
        """
         Input : Time range
         Output : Entries in bsm_agg_data table
        """
        table = self._db.agg_table
        agg_data = self.calculate_aggregates_single_fetch(from_time, to_time)
        
        with table.batch_writer() as batch:
            for index, row in agg_data.iterrows():
                batch.put_item(json.loads(row.to_json(), parse_float=Decimal))

    # This function will retrieve and insert the aggregates for each sensor type
    # (HeartRate, Temperature, SPO2) and for each device and insert it into bsm_agg_data
    def insert_aggregates(self, from_time, to_time):
        """
         Input : Time range
         Output : Entries in bsm_agg_data table
        """
        table = self._db.agg_table
        start_time = datetime.datetime.strptime(from_time, '%Y-%m-%d %H:%M:%S')
        start_time = start_time - datetime.timedelta(minutes=start_time.minute % 1,
                             seconds=start_time.second)
        end_time = datetime.datetime.strptime(to_time, '%Y-%m-%d %H:%M:%S')
        end_time = end_time - datetime.timedelta(minutes=end_time.minute % 1,
                             seconds=end_time.second)
        print(start_time)
        print(end_time)
        delta = datetime.timedelta(minutes=1)
        agg_data_list = pd.DataFrame()
        while start_time < end_time:
            to_time = start_time + delta
            agg_data = self.calculate_aggregates_per_sensor_per_minute(start_time.strftime('%Y-%m-%d %H:%M:%S'), to_time.strftime('%Y-%m-%d %H:%M:%S'))
            agg_data_list = pd.concat([agg_data_list, agg_data])
            start_time += delta
        
        with table.batch_writer() as batch:
            for index, row in agg_data_list.iterrows():
                batch.put_item(json.loads(row.to_json(), parse_float=Decimal))

  
        
class AnomalyDetection:
    def __init__(self):
        self._db = Database()
        self._latest_error = ''     
        self.base_table = 'bsm_data'
        self.agg_table = 'bsm_agg_data'
        self.alerts_table = 'bsm_alerts'

        with open('config.json', 'r') as config_file:
            self.config_data = json.load(config_file)
        print("Anomaly Detection Rules\n")
        print(self.config_data)

    # Retrieve aggrgations from bsm_agg_data table within the given timerange
    def get_agg_data(self, from_time, to_time):
        """
         Input : Time range
         Output : Aggregates per device per sensor type
        """
        table = self._db.agg_table
        scan_kwargs = {
            'FilterExpression': Key('timestamp').between(from_time, to_time),
            'ProjectionExpression': "#ts, deviceid, #max_value, #mean_value, #min_value, datatype",
            'ExpressionAttributeNames': {"#ts": "timestamp", "#max_value": "max", "#min_value": "min", "#mean_value": "mean"}
        }
        data = []
        done = False
        start_key = None
        while not done:
            if start_key:
                scan_kwargs['ExclusiveStartKey'] = start_key
            response = table.scan(**scan_kwargs)
            for i in response['Items']:
                data.append(json.dumps(i, cls=DecimalEncoder))
            start_key = response.get('LastEvaluatedKey', None)
            done = start_key is None
        return data

    # Runs the rules defined in the config.json file and detects the anomolies
    # The detected animolies are stored in bsm_alerts table
    def run_rule(self,  from_time, to_time):
        """
         Input : Time range
         Output : bsm_alerts table will be populated with anomalies
        """
        data = self.get_agg_data(from_time, to_time)
        value_list = []
        for d in data:
            record = json.loads(d)
            value_list.append(record)
        # print(value_list)
        count = {}
        count['Temperature'] = 0
        count['HeartRate'] = 0
        count['SPO2'] = 0
        print("\n\nAnolmalies\n\n")

        for record in value_list:          
            if record['mean'] < self.config_data[record['datatype']]['avg_min'] or record['mean'] > self.config_data[record['datatype']]['avg_max']:
                count[record['datatype']] +=1
                if count[record['datatype']] >= self.config_data[record['datatype']]['trigger_count']:
                    print(record)
                    self._db.alerts_put_item(record)
                    count[record['datatype']] = 0
            else:
                count[record['datatype']] = 0