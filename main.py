"""
This is the main program for bedside monitoring data aggregation and anomaly detection
This main program assumes that data is already populated in bsm_data dynamodb table
To populate bsm_data run the BedSideMonitor.py script which takes device_id as a parameter
"""

from model import MinutelyAggregateModel, AnomalyDetection
from datetime import datetime

aggregate = MinutelyAggregateModel()

from_time = '2021-07-18 00:00:00'
to_time = '2021-07-18 01:00:00'
aggregate.insert_aggregates_single_fetch(from_time, to_time)

anomolies = AnomalyDetection()
anomolies.run_rule(from_time, to_time)