# Iot Bed Side Monitoring


# Introduction

In this project, we have built a bed side monitor using IoT Core and DynamoDB lab.

The device simulator will publish Heart Rate, SpO2 (Oxygen Saturation) and Body Temperature values periodically. we’ll need to channel that to the cloud (Iot Core → DynamoDB), and then aggregate data and detect anomalies.
